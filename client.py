#!/usr/bin/env python

import asyncio
import websockets

import sys
import time
import random

async def get_massage():
    await asyncio.sleep(random.random() * 5)
    return "rand {}".format(random.randint(100, 999))


async def hello():
    async with websockets.connect('ws://127.0.0.1:5678') as websocket:
        while True:
            #name = input("What's your name? ")
            name = await get_massage()
            print("> {}".format(name))
            await websocket.send(name)
            # print("> {}".format(name))

            greeting = await websocket.recv()
            print("< {}".format(greeting))
            pass
while True:
    try:
        asyncio.get_event_loop().run_until_complete(hello())
    except ConnectionRefusedError as ex:
        time.sleep(1)
        print("."),
        sys.stdout.flush()
        pass
    except Exception as ex:
        print(type(ex))

