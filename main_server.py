
#!/usr/bin/env python

import asyncio
import datetime
import random
import websockets


from functools import wraps

import asyncio
from contextlib import suppress


class PeriodicRun():
    def __init__(self, func, time):
        self.func = func
        self.time = time
        self.is_started = False
        self._task = None

    async def start(self):
        if not self.is_started:
            self.is_started = True
            self._task = asyncio.ensure_future(self._run())

    async def stop(self):
        if self.is_started:
            self.is_started = False
            self._task.cancel()
            with suppress(asyncio.CancelledError):
                await self._task

    async def _run(self):
        while True:
            await asyncio.sleep(self.time)
            await self.func()

connected_lock = asyncio.Lock()
#connected = set()
connected = {}

from time import time
async def register(connection):
    global connected
    with await connected_lock:
        connected[connection] = int(time())
    #connected.add(connection)
    #print ("+ REGISTER")


async def release(connection):
    global connected
    with await connected_lock:
        #connected.remove(connection)
        try:
            del connected[connection]
            #print("- RELEASE")
        except:
            pass


#check_lock = asyncio.Lock()

from datetime import *
from time import *
async def connection_checker():

    global connected

    with await connected_lock:
        print("CEHCKING OK {} ".format(len(connected)))
        now = int(time())
        need_to_be_closed = []
        for connection, timestamp in connected.items():
            print("{:>4} = {}-{}".format(now - timestamp, now, timestamp))
            if now - timestamp > 2:
                #asyncio.ensure_future(connection.close())
                need_to_be_closed.append(connection)
                # print ("...... going to Close.... [{}]".format(connection.state_name))
            pass

        pass

        for connection in need_to_be_closed:
            del connected[connection]
            asyncio.ensure_future(connection.close())
            print("...... going to Close.... [{}]".format(connection.state_name))


    #print("CEHCKING OK {}".format(random.random() * 3))
    pass


async def producer():
    return str(random.randint(100, 999))


async def consumer(data):
    data = "RECEIVED [{}]".format(data)
    print (data)
    return data


async def handler(websocket, path):
    await PeriodicRun(connection_checker, 3).start()
    while True:
        try:

            await register(websocket)

            listener_task = asyncio.ensure_future(websocket.recv())
            done, pending = await asyncio.wait(
                [listener_task],
                return_when=asyncio.FIRST_COMPLETED)

            if listener_task in done:
                message = listener_task.result()
                data = await consumer("{} {}".format(message, websocket.remote_address))
                await websocket.send(data)
            else:
                listener_task.cancel()
                print("CLOSSSSE")

        except websockets.ConnectionClosed:

            try:
                listener_task.cancel()
                print("CLOSED")
            except Exception as ex:
                print ("EXCEPTION: {}".format(ex))
            pass
        finally:
            await release(websocket)


start_server = websockets.serve(handler, '127.0.0.1', 5678)
#start_server.wait_closed()

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()

